package main

import (
	"fmt"
	"os"
	"os/exec"
	"strconv"

	"github.com/go-vgo/robotgo"
)
func MyMouseClick(x int, y int, arg string) {
	robotgo.MoveMouseSmooth(x, y, 1.0, 0.0)
	robotgo.MouseClick(arg, false)
}
func IsAttacking() bool {
	color := robotgo.GetPixelColor(1018, 97)
	switch color {
	case "ff0000":
		return true
	case "fe0000":
		return true
	case "ff8080":
		return true
	default:
		return false
	}
}
func FocusClient() {
	exec.Command("wmctrl", "-a", "Tibia -").Run()
}
func Connected() bool {
	OnlineStorePointerX := 1281
	OnlineStorePointerY := 372
	color := robotgo.GetPixelColor(OnlineStorePointerX, OnlineStorePointerY)
	switch color {
	case "1d4364":
		return true
	case "1c4161":
		return true
	default:
		return false
	}
}
func IsMobAround() bool {
	BattleMobPointerXInit := 1012
	BattleMobPointerYInit := 97
	BattleMobPointerXEnd := 1160 - BattleMobPointerXInit
	BattleMobPointerYEnd := 140 - BattleMobPointerYInit
	i, j := robotgo.FindBitmap(robotgo.OpenBitmap("battle-clear.png"), robotgo.CaptureScreen(BattleMobPointerXInit, BattleMobPointerYInit, BattleMobPointerXEnd, BattleMobPointerYEnd))
	fmt.Println(i, j)
	if i == -1 || j == -1 {
		return true
	} else {
		return false
	}
}
func AtWaypoint(waypoint string) bool {
	CenteredMapX := 50
	CenteredMapY := 52
	MapInitPosX := 1192
	MapInitPosy := 58
	MapEndPosX := 1298
	MapEndPosY := 167
	fx, fy := robotgo.FindBitmap(robotgo.OpenBitmap(waypoint), robotgo.CaptureScreen(MapInitPosX, MapInitPosy, MapEndPosX-MapInitPosX, MapEndPosY-MapInitPosy))
	if fx == CenteredMapX && fy == CenteredMapY {
		return true
	} else {
		return false
	}
}
func FindOnMinimap(waypoint string) (int, int) {
	MapInitPosX := 1192
	MapInitPosy := 58
	MapEndPosX := 1298
	MapEndPosY := 167
	fx, fy := robotgo.FindBitmap(robotgo.OpenBitmap(waypoint), robotgo.CaptureScreen(MapInitPosX, MapInitPosy, MapEndPosX-MapInitPosX, MapEndPosY-MapInitPosy))
	return fx, fy
}
func Loot() {
	pos := [][]int{{655, 277}, {673, 325}, {672, 333}, {665, 368}, {627, 360}, {582, 359}, {579, 317}, {575, 283}}
	robotgo.KeyToggle("shift", "down")
	for _, i := range pos {
		robotgo.MoveClick(i[0], i[1], "left")
	}
	robotgo.KeyToggle("shift", "up")
}
func GetStatusPercentage(status string) float64 {
	hp := []string{"d34f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "c24a4a"}
	mp := []string{"6260c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "38369c"}
	list := hp
	y := 0
	if status == "hp" {
		list = hp
		y = 179
	} else {
		list = mp
		y = 191
	}
	fmt.Println(list)
	for k, v := range list {
		if robotgo.GetPixelColor(1207+k, y) == v {
		} else {
			fmt.Println(k)
			fmt.Println(robotgo.GetPixelColor(1207+k, y))
			return float64(k) * 0.92
		}
	}
	return 100.0
}
func ItemCount(item string, slots int) int {
	StartPosX := 0
	StartPosY := 471
	_item := robotgo.OpenBitmap(item)
	count := 0
	slots = slots / 4
	stack := IsStackable(item)
	var size int
	if stack {
		size = 21
	} else {
		size = 34
	}
	for i := 0; i < slots; i++ {
		StartPosX = 1195
		for j := 0; j < 4; j++ {
			temp := robotgo.CaptureScreen(StartPosX, StartPosY, 34, size)
			a, b := robotgo.FindBitmap(_item, temp)
			if a >= 0 && b >= 0 {
				count++
			}
			StartPosX += 37
			defer robotgo.FreeBitmap(temp)
		}
		StartPosY += 37
	}
	defer robotgo.FreeBitmap(_item)
	return count
}
func IsStackable(it string) bool {
	switch it {
	case "rope.png":
		return false
	case "leatherboots.png":
		return false
	case "scimitar":
		return false
	case "fish.png":
		return true
	case "arrow2.png":
		return true
	case "meat2.png":
		return true
	case "longsowrd.png":
		return false
	default:
		return false
	}
}
func ItemDrop(item []string, backpackSlots int, ammountLeft int) {
	BpInitPosX := 1185
	BpEndPosX := 1355
	BpInitPosY := 452
	BpEndPosY := 759
	CharCenteredX := 618
	CharCenteredY := 324
	for v := range item {
		tofind := robotgo.OpenBitmap(item[v])
		for ItemCount(item[v], backpackSlots) > ammountLeft {
			MyBitmap := robotgo.CaptureScreen(BpInitPosX, BpInitPosY, BpEndPosX-BpInitPosX, BpEndPosY-BpInitPosY)
			fx, fy := robotgo.FindBitmap(tofind, MyBitmap)
			robotgo.MoveMouse(BpInitPosX+fx+17, BpInitPosY+fy+17)
			robotgo.MouseToggle("down")
			robotgo.MoveMouse(CharCenteredX, CharCenteredY)
			robotgo.MouseToggle("up")
			defer robotgo.FreeBitmap(MyBitmap)
			robotgo.MicroSleep(400)
		}
		defer robotgo.FreeBitmap(tofind)
	}
}
func HasHigher() bool {
	InitBackPosX := 1319 //17
	InitBackPosY := 452 //15
	myBitmap := robotgo.CaptureScreen(InitBackPosX, InitBackPosY, 17, 15)
	hasBack := robotgo.OpenBitmap("backsign.png")
	a, b := robotgo.FindBitmap(hasBack, myBitmap)
	defer robotgo.FreeBitmap(myBitmap)
	defer robotgo.FreeBitmap(hasBack)
	if a >= 0 && b >= 0 {
		return true
	} else {
		return false
	}
}
func OpenNextBackpack(backpack string) {
	BpInitPosX := 1185
	BpEndPosX := 1355
	BpInitPosY := 452
	BpEndPosY := 759
	tofind := robotgo.OpenBitmap(backpack)
	if ItemCount(backpack, 20) > 0 {
		MyBitmap := robotgo.CaptureScreen(BpInitPosX, BpInitPosY, BpEndPosX-BpInitPosX, BpEndPosY-BpInitPosY)
		fx, fy := robotgo.FindBitmap(tofind, MyBitmap)
		MyMouseClick(BpInitPosX+fx+17, BpInitPosY+fy+17, "right")
		defer robotgo.FreeBitmap(tofind)
		defer robotgo.FreeBitmap(MyBitmap)
		robotgo.MicroSleep(400)
	}
}
func OpenPreviousBackpack() {
	InitBackPosX := 1319 //17
	InitBackPosY := 452 //15
	myBitmap := robotgo.CaptureScreen(InitBackPosX, InitBackPosY, 17, 15)
	hasBack := robotgo.OpenBitmap("backsign.png")
	a, b := robotgo.FindBitmap(hasBack, myBitmap)
	defer robotgo.FreeBitmap(myBitmap)
	defer robotgo.FreeBitmap(hasBack)
	if HasHigher() {
		MyMouseClick(InitBackPosX+a+3, InitBackPosY+b+3, "left")
	}
}
func main() {
	argv := os.Args[1:][0]
	if argv == "pos" {
		x, y := robotgo.GetMousePos()
		fmt.Println("pos: ", x, y)
		color := robotgo.GetPixelColor(x, y)
		fmt.Println("color---- ", color)
	} else if argv == "move" {
		tempx, _ := strconv.Atoi(os.Args[1:][1])
		tempy, _ := strconv.Atoi(os.Args[1:][2])
		robotgo.MoveMouseSmooth(tempx, tempy, 1.0, 0.0)
	} else if argv == "attacking" {
		fmt.Println(IsAttacking())
	} else if argv == "battle" {
		fmt.Println(IsMobAround())
	} else if argv == "loot" {
		FocusClient()
		robotgo.Sleep(2)
		Loot()
	} else if argv == "connected" {
		fmt.Println(Connected())
	} else if argv == "atway" {
		fmt.Println(AtWaypoint(os.Args[1:][1]))
	} else if argv == "capture" {
		z, _ := strconv.Atoi(os.Args[1:][1])
		x, _ := strconv.Atoi(os.Args[1:][3])
		c, _ := strconv.Atoi(os.Args[1:][2])
		v, _ := strconv.Atoi(os.Args[1:][4])
		bitmap := robotgo.CaptureScreen(z, x, c-z, v-x)
		robotgo.SaveBitmap(bitmap, "output.png")
	} else if argv == "findmap" {
		fmt.Println(FindOnMinimap(os.Args[1:][1]))
	} else if argv == "hp" {
		fmt.Println(GetStatusPercentage("hp"))
	} else if argv == "mp" {
		fmt.Println(GetStatusPercentage("mp"))
	} else if argv == "color" {
		x, _ := strconv.Atoi(os.Args[1:][1])
		y, _ := strconv.Atoi(os.Args[1:][2])
		fmt.Println(robotgo.GetPixelColor(x, y))
	} else if argv == "count" {
		x, _ := strconv.Atoi(os.Args[1:][2])
		fmt.Println(ItemCount(os.Args[1:][1], x))
	} else if argv == "drop" {
		FocusClient()
		robotgo.Sleep(2)
		var todrop = []string {"rope.png", "leatherboots.png", "arrow2.png"}
		ItemDrop(todrop, 8, 0)
	} else if argv == "higher" {
		fmt.Println(HasHigher())
	} else if argv == "opennext" {
		OpenNextBackpack(os.Args[1:][1])
	} else if argv == "openprevious" {
		OpenPreviousBackpack()
	}
}
