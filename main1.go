package main

import (
	"fmt"
	"math/rand"
	"os"
	"os/exec"
	"time"

	"github.com/go-vgo/robotgo"
)

func makeTimestamp() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}
func FocusClient() {
	exec.Command("wmctrl", "-a", "Tibia -").Run()
}
func Connected() bool {
	OnlineStorePointerX := 1281
	OnlineStorePointerY := 372
	color := robotgo.GetPixelColor(OnlineStorePointerX, OnlineStorePointerY)
	switch color {
	case "1d4364":
		return true
	case "1c4161":
		return true
	default:
		return false
	}
}
func MyMouseClick(x int, y int, arg string) {
	robotgo.MoveMouseSmooth(x, y, 1.0, 0.0)
	robotgo.MouseClick(arg, false)
}
func IsAttacking() bool {
	BattleAttackingMobX := 1022
	BattleAttackingMobY := 116
	color := robotgo.GetPixelColor(BattleAttackingMobX, BattleAttackingMobY)
	switch color {
	case "ff0000":
		return true
	case "fe0000":
		return true
	case "ff8080":
		return true
	default:
		return false
	}
}
func IsMobAround() bool {
	BattleMobPointerXInit := 1012
	BattleMobPointerYInit := 97
	BattleMobPointerXEnd := 1160 - BattleMobPointerXInit
	BattleMobPointerYEnd := 140 - BattleMobPointerYInit
	MyBitmap := robotgo.OpenBitmap("battle-clear.png")
	_MyBitmap := robotgo.CaptureScreen(BattleMobPointerXInit, BattleMobPointerYInit, BattleMobPointerXEnd, BattleMobPointerYEnd)
	i, j := robotgo.FindBitmap(MyBitmap, _MyBitmap)
	defer robotgo.FreeBitmap(MyBitmap)
	defer robotgo.FreeBitmap(_MyBitmap)
	if i == -1 || j == -1 {
		return true
	} else {
		return false
	}
}
func Attack() {
	BattleEdgeX := rand.Intn(1030-1013) + 1013
	BattleEdgeY := rand.Intn(113-98) + 98
	MyMouseClick(BattleEdgeX, BattleEdgeY, "left")
	CenterCursor()
	robotgo.MilliSleep(100)
}
func Loot() {
	pos := [][]int{{627, 271}, {673, 276}, {671, 316}, {671, 361}, {628, 371}, {581, 375}, {581, 328}, {574, 275}}
	robotgo.KeyToggle("shift", "down")
	for _, i := range pos {
		MyMouseClick(i[0], i[1], "right")
		robotgo.MilliSleep(100)
	}
	robotgo.KeyToggle("shift", "up")
}
func Stop() {
	robotgo.KeyTap("esc")
}
func AtWaypoint(waypoint string) bool {
	CenteredMapX := 50
	//old 49
	CenteredMapY := 53
	//old 53
	MapInitPosX := 1192
	MapInitPosy := 58
	MapEndPosX := 1298
	MapEndPosY := 167
	MyBitmap := robotgo.CaptureScreen(MapInitPosX, MapInitPosy, MapEndPosX-MapInitPosX, MapEndPosY-MapInitPosy)
	WayBitmap := robotgo.OpenBitmap(waypoint)
	fx, fy := robotgo.FindBitmap(robotgo.OpenBitmap(waypoint), MyBitmap)
	defer robotgo.FreeBitmap(MyBitmap)
	defer robotgo.FreeBitmap(WayBitmap)
	if fx == CenteredMapX && fy == CenteredMapY {
		return true
	} else {
		return false
	}
}
func FindWaypointPos(waypoint string) (int, int) {
	MapInitPosX := 1192
	MapInitPosy := 58
	MapEndPosX := 1298
	MapEndPosY := 167
	MyBitmap := robotgo.CaptureScreen(MapInitPosX, MapInitPosy, MapEndPosX-MapInitPosX, MapEndPosY-MapInitPosy)
	WayBitmap := robotgo.OpenBitmap(waypoint)
	rx, ry := robotgo.FindBitmap(WayBitmap, MyBitmap)
	defer robotgo.FreeBitmap(MyBitmap)
	defer robotgo.FreeBitmap(WayBitmap)
	return rx, ry
}
func CenterCursor() {
	InitEdgeX := 256
	InitEdgeY := 60
	EndEdgeX := 995
	EndEdgeY := 601
	FinalRandX := rand.Intn(EndEdgeX-InitEdgeX) + InitEdgeX
	FinalRandY := rand.Intn(EndEdgeY-InitEdgeY) + InitEdgeY
	robotgo.MoveMouseSmooth(FinalRandX, FinalRandY, 1.0, 0.0)
}
func Reverse(numbers []string) []string {
	for i, j := 0, len(numbers)-1; i < j; i, j = i+1, j-1 {
		numbers[i], numbers[j] = numbers[j], numbers[i]
	}
	return numbers
}
func GetStatusPercentage(status string) float64 {
	hp := []string{"d34f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "c24a4a"}
	mp := []string{"6260c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "38369c"}
	list := hp
	y := 0
	if status == "hp" {
		list = hp
		y = 179
	} else {
		list = mp
		y = 191
	}
	for k, v := range list {
		if robotgo.GetPixelColor(1207+k, y) == v {
		} else {
			return float64(k) * 0.92
		}
	}
	return 100.0
}
func GetStatusPercentage(status string) float64 {
	hp := []string{"d34f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "db4f4f", "c24a4a"}
	mp := []string{"6260c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "4340c0", "38369c"}
	list := hp
	y := 0
	if status == "hp" {
		list = hp
		y = 179
	} else {
		list = mp
		y = 191
	}
	fmt.Println(list)
	for k, v := range list {
		if robotgo.GetPixelColor(1207+k, y) == v {
		} else {
			fmt.Println(k)
			fmt.Println(robotgo.GetPixelColor(1207+k, y))
			return float64(k) * 0.92
		}
	}
	return 100.0
}
func ItemCount(item string, slots int) int {
	StartPosX := 0
	StartPosY := 471
	_item := robotgo.OpenBitmap(item)
	count := 0
	slots = slots / 4
	stack := IsStackable(item)
	var size int
	if stack {
		size = 21
	} else {
		size = 34
	}
	for i := 0; i < slots; i++ {
		StartPosX = 1195
		for j := 0; j < 4; j++ {
			temp := robotgo.CaptureScreen(StartPosX, StartPosY, 34, size)
			a, b := robotgo.FindBitmap(_item, temp)
			if a >= 0 && b >= 0 {
				count++
			}
			StartPosX += 37
			defer robotgo.FreeBitmap(temp)
		}
		StartPosY += 37
	}
	defer robotgo.FreeBitmap(_item)
	return count
}
func IsStackable(it string) bool {
	switch it {
	case "rope.png":
		return false
	case "leatherboots.png":
		return false
	case "scimitar":
		return false
	case "fish.png":
		return true
	case "arrow2.png":
		return true
	case "meat2.png":
		return true
	case "longsword.png":
		return false
	case "mace.png":
		return false
	case "swampclub.png":
		return false
	case "redbackpack.png":
		return false
	case "emptyslot.png":
		return false
	default:
		return false
	}
}
func ItemDrop(item []string, backpackSlots int, ammountLeft int) {
	BpInitPosX := 1185
	BpEndPosX := 1355
	BpInitPosY := 452
	BpEndPosY := 759
	CharCenteredX := 618
	CharCenteredY := 324
	for v := range item {
		tofind := robotgo.OpenBitmap(item[v])
		for ItemCount(item[v], backpackSlots) > ammountLeft {
			MyBitmap := robotgo.CaptureScreen(BpInitPosX, BpInitPosY, BpEndPosX-BpInitPosX, BpEndPosY-BpInitPosY)
			fx, fy := robotgo.FindBitmap(tofind, MyBitmap)
			robotgo.MoveMouse(BpInitPosX+fx+17, BpInitPosY+fy+17)
			robotgo.MouseToggle("down")
			robotgo.MoveMouse(CharCenteredX, CharCenteredY)
			robotgo.MouseToggle("up")
			defer robotgo.FreeBitmap(MyBitmap)
			robotgo.MicroSleep(400)
		}
		defer robotgo.FreeBitmap(tofind)
	}
}
func HasHigher() bool {
	InitBackPosX := 1319 //17
	InitBackPosY := 452  //15
	myBitmap := robotgo.CaptureScreen(InitBackPosX, InitBackPosY, 17, 15)
	hasBack := robotgo.OpenBitmap("backsign.png")
	a, b := robotgo.FindBitmap(hasBack, myBitmap)
	defer robotgo.FreeBitmap(myBitmap)
	defer robotgo.FreeBitmap(hasBack)
	if a >= 0 && b >= 0 {
		return true
	} else {
		return false
	}
}
func HunLooTar() {
	// configurable per script
	//wpts := []string{"1.png", "2.png", "3.png", "4.png"}
	//wpts := []string{"1.png", "2.png", "3.png", "4.png", "5.png", "6.png", "7.png", "8.png", "9.png", "10.png"}
	wpts := []string{"1.png", "2.png", "3.png", "4.png", "5.png", "6.png", "7.png", "8.png"}
	index := 0
	foodhotkey := "f7"
	reverse := false
	//hardcoded stuff -- DO NOT TOUCH --
	MapInitPosX := 1192
	MapInitPosy := 58
	walkingnow := true
	lootingnow := false
	AllowedToClick := true
	walkstamp := makeTimestamp()
	foodstamp := makeTimestamp()
	foodrand := 0

	FocusClient()
	for {
		//targeting
		if !lootingnow {
			if IsMobAround() && !IsAttacking() {
				if walkingnow {
					Stop()
					walkingnow = false
				}
				Attack()
				lootingnow = true
			}
		}
		//*** looting
		if lootingnow && !IsAttacking() {
			fmt.Println("looting...")
			Loot()
			time.Sleep(200)
			lootingnow = false
			if makeTimestamp()-foodstamp >= int64(foodrand)*1000 {
				fmt.Println("Eating food")
				for i := 1; i < (rand.Intn(10-2) + 2); i++ {
					robotgo.KeyTap(foodhotkey)
					robotgo.Sleep(1)
				}
				foodrand = rand.Intn(200-80) + 80
				foodstamp = makeTimestamp()
			}
			if !IsMobAround() {
				walkingnow = true
				AllowedToClick = true
			}
		}
		//walking
		if walkingnow {
			if !AtWaypoint(wpts[index]) && AllowedToClick {
				x, y := FindWaypointPos(wpts[index])
				MyMouseClick(MapInitPosX+x+5, MapInitPosy+y+3, "left")
				CenterCursor()
				AllowedToClick = false
			} else if AtWaypoint(wpts[index]) {
				if index == len(wpts)-1 {
					if reverse {
						wpts = Reverse(wpts)
						index = 1
					} else {
						index = 0
					}
					walkstamp = makeTimestamp()
					AllowedToClick = true
				} else {
					index += 1
					walkstamp = makeTimestamp()
					AllowedToClick = true
				}
			} else if !AtWaypoint(wpts[index]) && !AllowedToClick {
				if makeTimestamp()-walkstamp >= 30000 {
					x, y := FindWaypointPos(wpts[index])
					if x == -1 || y == -1 {
						if index == 0 {
							index = len(wpts) - 1
						} else {
							index -= 1
						}
					}
					walkstamp = makeTimestamp()
					AllowedToClick = true
				}
			}
		}
		//close for
		if !Connected() {
			//break
			os.Exit(3)
		}
	}
}
func Healing() {
	HpHealerEnabled := true
	PotHealerEnabled := false
	//ManaHealherEnabled := false
	SpellHealingEnabled := true
	TrainingEnabled := true
	PotHealerHotkey := "f1"
	//ManaHealerHotkey := "f2"
	SpellHealingHotkey := "f3"
	TrainingHotkey := "f3"
	//from % to %
	PotHppc := []float64{float64(40), float64(85)}
	//	MpMppc := []float64{float64(40), float64(95)}
	TrainFromTo := []float64{float64(100), float64(50)}
	//from % to % and minimum mana %
	SpellHppcMp := []float64{float64(30), float64(60), float64(10)}

	// hardcoded stuff
	healingNow := false
	for {
		if HpHealerEnabled {
			if PotHealerEnabled {
				if PotHppc[0] >= GetStatusPercentage("hp") {
					for GetStatusPercentage("hp") < PotHppc[1] {
						robotgo.KeyTap(PotHealerHotkey)
						robotgo.MilliSleep(500)
					}
				}
			}
			if SpellHealingEnabled {
				if !healingNow && SpellHppcMp[0] >= GetStatusPercentage("hp") && GetStatusPercentage("mp") >= SpellHppcMp[2] {
					healingNow = true
				} else if healingNow && GetStatusPercentage("hp") < SpellHppcMp[1] && GetStatusPercentage("mp") >= SpellHppcMp[2] {
					robotgo.KeyTap(SpellHealingHotkey)
					robotgo.MilliSleep(300)
				} else if healingNow && (GetStatusPercentage("mp") < SpellHppcMp[2] || GetStatusPercentage("hp") >= SpellHppcMp[1]) {
					healingNow = false
				}
			}
			if TrainingEnabled {
				if GetStatusPercentage("mp") >= TrainFromTo[0] {
					for GetStatusPercentage("mp") > TrainFromTo[1] {
						robotgo.KeyTap(TrainingHotkey)
						robotgo.MilliSleep(200)
					}
				}
			}
		}
		if !Connected() {
			//break
			os.Exit(3)
		}
	}
}
func main() {
	go func() {
		HunLooTar()
	}()
	Healing()
	// close main
}
